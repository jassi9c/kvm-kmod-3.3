#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<linux/kvm.h>
#include<fcntl.h> 

#define BLOCK_SIZE 128

struct dirty_log_info {
	int index ; 
	unsigned long size;
	unsigned long *dirty_bitmap;
};

struct histogram_data {
	unsigned long  max_delta_for_block;
	unsigned long max_block_value;
	int index;
	unsigned long page[BLOCK_SIZE];
}__attribute__((packed));

int compare(const void *a, const void *b)
{
	struct histogram_data *d1 = (struct histogram_data *)a;
	struct histogram_data *d2 = (struct histogram_data *)b;
	return d2->max_block_value - d1->max_block_value;
}

int compare2(const void *a, const void *b)
{
	struct histogram_data *d1 = (struct histogram_data *)a;
	struct histogram_data *d2 = (struct histogram_data *)b;
	return d2->max_delta_for_block - d1->max_delta_for_block;
}


static inline unsigned long find_index(struct histogram_data *d, int index)
{
	int i;
	for(i = 0; i < BLOCK_SIZE; i++)
		if(index == d[i].index)
			return d[i].max_block_value;
}

int main()
{
	printf("size is : %d\n",sizeof(struct histogram_data));
	int fd , index ; 
	fd = open("/dev/kvm",O_RDWR);
	if(fd==-1)
		return -1;
	char name[]="modified kvm on the rocks";
	index = ioctl(fd , KVM_GET_VM_INDEX , &name);
	printf("index = %d\n",index);

	struct dirty_log_info temp ; 
	temp.index=index;
	temp.size=0;
	int r = ioctl(fd , KVM_GET_DIRTY_LOG_SIZE_BY_VM_INDEX , &temp);
	printf("size = %lu\n",temp.size);

	temp.dirty_bitmap = malloc(temp.size*sizeof(unsigned long));

	r = ioctl(fd, KVM_GET_DIRTY_LOG_BY_VM_INDEX , &temp);
	unsigned long int active = 0;
	int i;
	int j;
	unsigned long page_freq_value;
	unsigned long elements = temp.size / BLOCK_SIZE;
	unsigned long hist_data_size = elements * sizeof(struct histogram_data);
	
	for(i=0;i<temp.size;i++)
		if(temp.dirty_bitmap[i])
			active++;
	printf("active = %lu\n",active);
	
	struct histogram_data *d_old;
	struct histogram_data *d_new;
	
	d_old = (struct histogram_data *)malloc(hist_data_size);
	d_new = (struct histogram_data *)malloc(hist_data_size);
	
	memset(d_old, 0, hist_data_size);

	for(i = 0; i < elements; i++)
		d_old[i].index = i;
	/* 
	 * now the avg of the delta will be calculated and will be stored
	 * in max_block_value, if the value is -ve, then 0 will be stored
	 */

	for(i = 0;i < elements; i ++)
	{
		page_freq_value = 0;
		for(j=0; j < BLOCK_SIZE; j++)
		{
			d_new[i].page[j] = temp.dirty_bitmap[i * BLOCK_SIZE + j ];
			page_freq_value += temp.dirty_bitmap[i * BLOCK_SIZE + j ];
		}
		d_old[i].max_block_value = page_freq_value / BLOCK_SIZE; 
	}
	

#if 1
	while(1)
	//for(j = 0; j < 5; j++)
	{
		r = ioctl(fd, KVM_GET_DIRTY_LOG_BY_VM_INDEX , &temp);

		for(i = 0; i  < elements; i++)
			d_new[i].index = i;

		/* 
		 * now the avg of the delta will be calculated and will be stored
		 * in max_block_value, if the value is -ve, then 0 will be stored
		 */

		for(i = 0;i < elements; i ++ )
		{
			page_freq_value = 0;
			for(j=0; j < BLOCK_SIZE; j++)
				page_freq_value += (temp.dirty_bitmap[i * BLOCK_SIZE +j] 
						> d_old[i].page[j]) ? (temp.dirty_bitmap[i * 
							BLOCK_SIZE + j] - d_old[i].page[j]):0;
			d_new[i].max_block_value = page_freq_value / BLOCK_SIZE; 
		}

		for(i = 0; i < elements; i++)
			d_new[d_old[i].index].max_delta_for_block = d_new[d_old[i].index].max_block_value - d_old[i].max_block_value;

		qsort(d_new, elements,sizeof(struct histogram_data ), compare2);

		printf("delta change in %d is : \n",BLOCK_SIZE);
		for(i = 0; i < elements; i ++)
			printf("block %lu with dirtied pages %lu and delta change : %lu\n",d_new[i].index, d_new[i].max_block_value, d_new[i].max_delta_for_block);
		d_old = d_new;
		printf("\n\n NEW VALUE BEING FETCH!! \n\n");
		sleep(2);	
	}
#endif
	return 0;
}
